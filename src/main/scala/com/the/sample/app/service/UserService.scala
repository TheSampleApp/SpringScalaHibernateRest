package com.the.sample.app.service

import com.the.sample.app.model.User
import com.the.sample.app.repository.UserRepository
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

import java.util.Optional

trait UserService {
  def findAll(page: Int, pageSize: Int): java.util.List[User]

  def findById(id: Long): Optional[User]

  def findByEmail(email: String): Optional[User]

  def save(user: User): Unit

  def deleteById(id: Long): Unit
}

@Service
@Transactional
class UserServiceImpl(userRepository: UserRepository) extends UserService {
  override def findAll(page: Int, pageSize: Int): java.util.List[User] =
    userRepository.findAll(PageRequest.of(page, pageSize)).toList

  override def findById(id: Long): Optional[User] = userRepository.findById(id)

  override def findByEmail(email: String): Optional[User] = userRepository.findByEmail(email)

  override def save(user: User): Unit = userRepository.save(user)

  override def deleteById(id: Long): Unit = userRepository.deleteById(id)
}
